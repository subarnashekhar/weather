package com.shriwatri.weather.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.shriwatri.weather.model.Forecast;



@Component
public class WeatherAdapterImpl implements WeatherAdapter {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherAdapterImpl.class);
	private static final String hourlyForecastEndpoint = "forecast/hourly";
	
	@Value("${weather.apiKey}")
	private String apiKey ;
	
	
	@Value("${weather.url}")
	private String url ;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public Forecast retriveweatherDetails(int hours, String zipCode)  {


		String url = String.format("%s%s?key=%s&units=I&hours=%d&postal_code=%s", this.url,
				hourlyForecastEndpoint, this.apiKey, hours, zipCode);
		LOGGER.info("urlWithQueryParams::" + url);
		Forecast forecast;

		try {
			
			
			forecast = restTemplate.getForObject(url, Forecast.class);
		} catch (Exception ex) {
			throw ex;
		}

		return forecast;
	}


	}
	
	

