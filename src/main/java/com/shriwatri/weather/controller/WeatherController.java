package com.shriwatri.weather.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

//import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;



import com.shriwatri.weather.service.WeatherService;
import com.shriwatri.weather.service.WeatherServiceImpl;
import com.shriwatri.weather.validator.PostalCodeValidator;
import com.shriwatri.weather.weatherconstants.WeatherConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.shriwatri.weather.model.Forecast;

@RestController
@Api(value = "forecast", description = "Operations pertaining to weather forecasts")
public class WeatherController {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(WeatherController.class);
	
	 
	 @Autowired
	private WeatherService weatherService;
	
	
	@GetMapping("/getWeather/{zipCode}")
	@ApiOperation(value = "Get next day forecast")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved weather forecast"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 415, message = "The file type uploaded is unsupported") })
	public ResponseEntity<Forecast> getWeatherDetails(@PathVariable String zipCode) {
		
		LOGGER.info("getForcastDetails()::Start"+zipCode); 
		
		 Forecast foreCast=null;
		 
//			if(WeatherValidator.postalCodeValidation(postalCode)) {
//				try {
//					   foreCast=weatherservice.getNextDayHourlyForecast(postalCode);
//					}
//				catch(Exception ex) {
//					LOGGER.error(Constants.EXCEPTION_SERVICE,ex.getLocalizedMessage());
//					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,Constants.INTERNAL_SERVER_ERROR,ex);
//				}
//				return new ResponseEntity<Forecast>(foreCast,HttpStatus.OK);
//			  }
//		 	else {
//					throw new ResponseStatusException(HttpStatus.BAD_REQUEST,Constants.INPUT_ERROR);
//				}
//		 }
		 
		 if(!PostalCodeValidator.postalCodeValidation(zipCode)) {
			 LOGGER.error("Postal Code entered is not Valid");
			 
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST,WeatherConstants.INPUT_ERROR);
		 }
		
		 try {
		
			 foreCast= weatherService.getWeatherDetails(zipCode);
		 
		 }
		 catch(Exception ex) {
			 
			 LOGGER.error(WeatherConstants.EXCEPTION_SERVICE,ex.getLocalizedMessage());
		throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,WeatherConstants.INTERNAL_SERVER_ERROR,ex);
			 
		 }
		 return new ResponseEntity<Forecast>(foreCast,HttpStatus.OK);
		
	}
	

}
