package com.shriwatri.weather.service;

import com.shriwatri.weather.model.Forecast;


public interface WeatherService {
	
	public Forecast getWeatherDetails(String zipCode);
	

}
