package com.shriwatri.weather.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.shriwatri.weather.adapter.WeatherAdapter;
import com.shriwatri.weather.model.Forecast;
import com.shriwatri.weather.model.Weather;
import com.shriwatri.weather.util.WeatherLocalDateTime;
import com.shriwatri.weather.util.WeatherTemperature;



@Component
public class WeatherServiceImpl implements WeatherService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherServiceImpl.class);
	
	@Autowired
	private WeatherAdapter weatherAdapter;
	
	
	@Override
	public Forecast getWeatherDetails(String zipCode) {
		// TODO Auto-generated method stub
		
		
		Forecast forecast =  weatherAdapter.retriveweatherDetails(48,zipCode);
		
		  LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(forecast.getTimezone()));

	        // Filter for the full 24 hours of weather forecast for the next day
	        List<Weather> weatherList = forecast.getWeatherList().stream()
	                .filter(weather -> weather.getLocalDateTime().toLocalDate().isAfter(localDateTime.toLocalDate()))
	                .limit(24).sorted(new WeatherLocalDateTime()).collect(Collectors.toList());
	        forecast.setWeatherList(weatherList);

	        // Determine the coolest hour of the day from the next day's forecast
	        LocalDateTime coolestHourOfDay = forecast.getWeatherList().stream().min(new WeatherTemperature())
	                .get().getLocalDateTime();
	        forecast.setCoolestHourOfDay(coolestHourOfDay);

	        return forecast;
	}

}
