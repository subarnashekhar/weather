package com.shriwatri.weather.weatherconstants;

public class WeatherConstants {
	

	public static final String EXCEPTION_SERVICE = "Exception Occrued While Invoking Service";
	public static final String INTERNAL_SERVER_ERROR = "There was a problem occured while calling service";
	public static final String INPUT_ERROR = "Invalid Input";
	public static final String URL_PATH = "/weather/forecast";
	public static final String NOT_FOUND = "Invalid Postal code or Weather details are not found";

}
